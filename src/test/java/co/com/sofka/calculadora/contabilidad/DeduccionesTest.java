package co.com.sofka.calculadora.contabilidad;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.*;

@SpringBootTest
public class DeduccionesTest {
    @Test
    public  void DeductionTest(){
        double value = 100;
        double deduction = 0.04;
        double  result = 4;
        Assert.assertEquals(result, Deducciones.deduce(value, deduction),0);
    }


}