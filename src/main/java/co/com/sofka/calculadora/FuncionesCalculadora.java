package co.com.sofka.calculadora;

import co.com.sofka.calculadora.contabilidad.*;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FuncionesCalculadora {

    public static Function<Mono<DatosCalculadoraSumar>, Mono<DatosCalculadoraSumar>> Suma = lista ->
            Mono.just(lista.block());

    public static Function<List<Integer>, Mono<Integer>> Restar = lista ->
            Mono.just(lista.stream().reduce(0,(restado, restador)-> (restador-restado))).flatMap(x -> Mono.just(x*(-1)));


    public static Function<Integer, Mono<List<DatosCalculadoraMultiplicar>>> TablaMultiplicar = factor -> {

        List<DatosCalculadoraMultiplicar> tablaFinal =  new ArrayList<>(10);

        IntStream.range(0, 10).forEach(digito -> tablaFinal.add(DatosCalculadoraMultiplicar.builder()
                                                                .datoMultiplicador(digito)
                                                                .datoMultiplicando(factor)
                                                                .resultado(digito*factor).build()));
        return Mono.just(tablaFinal);
    };

    public static final Function< List<Integer>, Mono<List<DatosCalculadoraAmortizacion>> > Amortizacion = lista -> {

        final Double INTERES = 0.01;

        List<DatosCalculadoraAmortizacion> anortizacion = new ArrayList<>();

        IntStream.rangeClosed(1,lista.get(0)).forEach(periodo -> anortizacion.add(DatosCalculadoraAmortizacion.builder().montoCredito(lista.get(1))
                                                                                   .numeroCuotas(lista.get(0))
                                                                                   .periodo(periodo)
                                                                                   .interes((lista.get(0) - (lista.get(1)/lista.get(0)) * (periodo-1)) * INTERES )
                                                                                   .amortizacionDelCapital((double)(lista.get(1)/lista.get(0)))
                                                                                   .cuota( (lista.get(0) - (lista.get(1)/lista.get(0)) * (periodo-1)) * INTERES + (lista.get(1)/lista.get(0)) )
                                                                                   .build()));
        return Mono.just(anortizacion);
    };

    public static final Function<Integer, Mono<DatosDeduccion>> deduccionSalario = r -> {

        return OperacionesCalculadora.DeduccionesFactory(r);
    };
}
