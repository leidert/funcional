package co.com.sofka.calculadora.contabilidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class DatosCalculadoraSumar {
    private final int numero1;
    private final int numero2;
    private final int resultado;
}
