package co.com.sofka.calculadora.contabilidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class DatosCalculadoraMultiplicar {
    private final double datoMultiplicador;
    private final double datoMultiplicando;
    private final double resultado;

}
