package co.com.sofka.calculadora.contabilidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class DatosDeduccion {
    private final double Salario;
    private final double AporteSaludEmpleado;
    private final double AporteSaludEmpleador;
    private final double  AportePensiónEmpleado;
    private final double  AportePensiónEmpleador;
    private final double  AporteRiesgos;
    private final double AporteCajaCompensación;
    private final double TotalSalario;
    private final double AporteFSP;
}
