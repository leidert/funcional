package co.com.sofka.calculadora.contabilidad;

import java.util.function.Function;

public class Deducciones {

    public static double deduce(double value, double deduction) {
        return value * deduction;
    }
}
