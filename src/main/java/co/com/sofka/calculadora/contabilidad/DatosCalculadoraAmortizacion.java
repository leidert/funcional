package co.com.sofka.calculadora.contabilidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class DatosCalculadoraAmortizacion {
    private final double numeroCuotas;
    private final Integer montoCredito;
    private final Integer periodo;
    private final double interes;
    private final double amortizacionDelCapital;
    private final double cuota;
}
