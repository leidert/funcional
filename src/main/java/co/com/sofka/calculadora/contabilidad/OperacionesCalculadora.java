package co.com.sofka.calculadora.contabilidad;

import co.com.sofka.calculadora.FuncionesCalculadora;
import reactor.core.publisher.Mono;


public class OperacionesCalculadora {


    public static Mono<DatosCalculadoraSumar> sumaFactory(Integer numero1, Integer numero2){
        return Mono.just(DatosCalculadoraSumar.builder().numero1(numero1).numero2(numero2).resultado(numero1+numero2).build());
    }

    public static Mono<DatosDeduccion> DeduccionesFactory(Integer salario){
        return Mono.just(DatosDeduccion.builder().Salario(salario)
                                                    .AporteSaludEmpleado(salario*0.04)
                                                    .AporteSaludEmpleador(salario*0.012)
                                                    .AportePensiónEmpleado(salario*0.04)
                                                    .AportePensiónEmpleador(salario*0.085)
                                                    .AporteCajaCompensación(salario*0.04)
                                                    .AporteRiesgos(salario*0.005)
                                                    .AporteFSP(salario*0.01)
                                                    .TotalSalario(TotalDeduccionSalario(salario)).build());
    }

    public static double TotalDeduccionSalario(Integer salario){
        return (salario+((salario*0.085)+(salario*0.012)))-((salario*0.04)+(salario*0.005)+(salario*0.04)+(salario*0.04)+(salario*0.01));
    }

    public static Mono<DatosCalculadoraAmortizacion>  AmortizacionFactory(Integer numberOfFees, Integer creditAmount){
        return Mono.just(DatosCalculadoraAmortizacion.builder().montoCredito(creditAmount)
                .numeroCuotas(numberOfFees).build());
    }

}
