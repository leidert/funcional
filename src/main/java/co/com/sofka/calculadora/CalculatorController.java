package co.com.sofka.calculadora;

import co.com.sofka.calculadora.contabilidad.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static co.com.sofka.calculadora.FuncionesCalculadora.*;

@RestController
@RequestMapping(value = "/api")
public class CalculatorController {

    @GetMapping("/suma")
    public Mono<DatosCalculadoraSumar> getSuma(Integer a, Integer b){
        //return Mono.just(Arrays.asList(a,b)).flatMap(Suma);
        return Mono.just(OperacionesCalculadora.sumaFactory(a,b)).flatMap(Suma);
    }

    @GetMapping("/resta")
    public Mono<Integer> getResta(Integer restado, Integer restador){
        return Mono.just(Arrays.asList(restado, restador)).flatMap(Restar);
    }

    @GetMapping("/salario")
        public Mono<Integer> Salario(Integer salario){
        return Mono.just(1000000);
    }
    @GetMapping(value = "/multiplicar")
    public Mono<List<DatosCalculadoraMultiplicar>> tablaMultiplicar (Integer a) {
        return Mono.just(a).flatMap(TablaMultiplicar);
    }

    @GetMapping(value = "/amortizacion")
    public Mono<List<DatosCalculadoraAmortizacion>> tablaAmortizacion (Integer numberOfFees, Integer creditAmount) {
        return Mono.just(Arrays.asList(numberOfFees, creditAmount)).flatMap(Amortizacion);
    }
    @GetMapping(value = "/deduccion")
    public Mono<DatosDeduccion> DeduccionSalario (Integer salario) {
        return Mono.just(salario).flatMap(deduccionSalario);
    }

}
